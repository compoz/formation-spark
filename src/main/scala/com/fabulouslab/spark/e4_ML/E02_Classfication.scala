package com.fabulouslab.spark.e4_ML

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.{LogisticRegression, RandomForestClassifier}
import org.apache.spark.ml.evaluation.{MulticlassClassificationEvaluator, RegressionEvaluator}
import org.apache.spark.ml.feature.{HashingTF, StandardScaler, Tokenizer, VectorAssembler}
import org.apache.spark.ml.regression.LinearRegression
import org.apache.spark.sql.SparkSession

object E02_Classfication {

  def main(args: Array[String]) {

    /**
      * Classfication
      *
      * Dans le but de créer un modèle de ML capable de prédire la note depuis un commentaire sur un produit :
      *
      * - Charger le fichier data/reviews_Baby_5.json et faire un show
      * - Spliter le dataset en train et test (80/20)
      * - En utilisant reviewText, créer un pipeline (HashingTF et LogisticRegression(maxIter=5, regParam=0.3, elasticNetParam=0.8)) pour prédire la note
      * - Évaluer accuracy du modèle
      * - Utiliser d'autres algorithmes de classification, pour améliorer le résultat
      * */

    val sparkSession = SparkSession.builder
      .master("local[5]")
      .appName("exo-1")
      .getOrCreate()
    val amazonReviews = sparkSession
      .read
      .json("src/main/resources/reviews_Baby_5.json")
      .withColumnRenamed("overall","label")

  }
}
