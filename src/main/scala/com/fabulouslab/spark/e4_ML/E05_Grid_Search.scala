package com.fabulouslab.spark.e4_ML
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.{LogisticRegression, RandomForestClassifier}
import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.evaluation.{BinaryClassificationEvaluator, MulticlassClassificationEvaluator, RegressionEvaluator}
import org.apache.spark.ml.feature.{HashingTF, StandardScaler, Tokenizer, VectorAssembler}
import org.apache.spark.ml.recommendation.ALS
import org.apache.spark.ml.regression.LinearRegression
import org.apache.spark.ml.tuning.{CrossValidator, ParamGridBuilder}
import org.apache.spark.sql.SparkSession

object E05_Grid_Search {

  def main(args: Array[String]) {

    val sparkSession = SparkSession.builder
      .master("local[5]")
      .appName("exo-1")
      .getOrCreate()

    val amazonReviews = sparkSession
      .read
      .json("src/main/resources/reviews_Baby_5.json")
      .withColumnRenamed("overall","label")

    val split = amazonReviews.randomSplit(Array(0.8,0.2), 1234)
    val (train, test) = (split(0), split(1))

    val tokenizer = new Tokenizer().setInputCol("reviewText").setOutputCol("words")

    val hashingTF = new HashingTF()
      .setInputCol("words")
      .setOutputCol("rawFeatures")
      .setNumFeatures(50)

    val rf = new RandomForestClassifier()
      .setFeaturesCol("rawFeatures")
      .setLabelCol("label")
      .setNumTrees(50)

    val pipeline = new Pipeline().setStages(Array(tokenizer, hashingTF, rf))
    val paramGrid = new ParamGridBuilder()
      .addGrid(hashingTF.numFeatures, Array(10, 100, 1000))
      .addGrid(rf.numTrees, Array(10, 30))
      .build()

    val cv = new CrossValidator()
      .setEstimator(pipeline)
      .setEvaluator(new MulticlassClassificationEvaluator())
      .setEstimatorParamMaps(paramGrid)
      .setParallelism(2)

    val cvModel = cv.fit(train)
    val predictions = cvModel.transform(test)

    predictions.select("label","reviewText", "prediction").show()
  }
}