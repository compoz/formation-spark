package com.fabulouslab.spark.e4_ML

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.{LogisticRegression, RandomForestClassifier}
import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.evaluation.{MulticlassClassificationEvaluator, RegressionEvaluator}
import org.apache.spark.ml.feature.{HashingTF, StandardScaler, Tokenizer, VectorAssembler}
import org.apache.spark.ml.regression.LinearRegression
import org.apache.spark.sql.SparkSession

object E03_Clustering {

  def main(args: Array[String]) {

    /**
      * Clustering
      *
      * Dans le but repérer les points plus intéréssant pour se positionner comme taxi :
      * - Charger le fichier data/uber.csv et faire un show
      * - En utilisant Lat et Lon, et avec un algorithme de K-mean, repérer les huit points les plus intéresants pour le positionnement des taxis
      * - Afficher les coordonnées des 8 centres
      * */

    val sparkSession = SparkSession.builder
      .master("local[5]")
      .appName("exo-1")
      .getOrCreate()
    val ubers = sparkSession
      .read
      .option("header","true")
      .option("inferSchema","true")
      .csv("src/main/resources/uber.csv")

  }
}
