package com.fabulouslab.spark.e4_ML

import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.{LogisticRegression, RandomForestClassifier}
import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.evaluation.{MulticlassClassificationEvaluator, RegressionEvaluator}
import org.apache.spark.ml.feature.{HashingTF, StandardScaler, Tokenizer, VectorAssembler}
import org.apache.spark.ml.recommendation.ALS
import org.apache.spark.ml.regression.LinearRegression
import org.apache.spark.sql.SparkSession

object E04_Recommender_systems {

  def main(args: Array[String]) {

    /**
      * Recommender systems
      *
      * Dans le but de constuire un moteur de recommandation de films :
      * - Chargez le fichier data/movielens_ratings.csv et faire un show
      * - Constuire un moteur de recommandation avec un algorithme ALS
      * - Calculer root mean square error (rmse)
      * */

    val sparkSession = SparkSession.builder
      .master("local[5]")
      .appName("exo-1")
      .getOrCreate()
    val movies = sparkSession
      .read
      .option("header","true")
      .option("inferSchema","true")
      .csv("src/main/resources/movielens_ratings.csv")

  }
}