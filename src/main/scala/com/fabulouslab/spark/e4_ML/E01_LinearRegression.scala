package com.fabulouslab.spark.e4_ML

import com.fabulouslab.spark.e3_dataset.E02_Dedouble.getLastVideo
import com.fabulouslab.spark.e3_dataset.Video
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.feature.{StandardScaler, VectorAssembler}
import org.apache.spark.sql.SparkSession
import org.apache.spark.ml.regression.{GBTRegressor, LinearRegression, RandomForestRegressor}

object E01_LinearRegression {

  def main(args: Array[String]) {

    /**
      * Dans le but de créer un modèle de ML capable de prédire la consomation d'énergie :
      *
      *     - Chargez le fichier data/CyclePowerPlantDataSet.csv et faire un show
      *     - Spliter le dataset en train et test (80/20)
      *     - En utilisant temperature, exhaust_vacuum, ambient_pressure et relative_humidity, créer un pipeline
      *     (VectorAssembler et LinearRegression(maxIter=2, regParam=0.1, elasticNetParam=0.8)) pour prédire energy_output
      *     - En utilisant de dataset de test calculer le rmse
      *     - En jouant avec maxIter et regParam, essayez d'améliorer la qualité des prédictions
      *     - Ajouter au pipeline un StandardScaler, quel est l'impact sur rmse ?
      *     - Tester d'autres types d'algorithme (RandomForestRegressor , GBTRegressor)
      * */

    val sparkSession = SparkSession.builder
      .master("local[3]")
      .appName("exo-1")
      .getOrCreate()
    import sparkSession.implicits._

    val ccpp = sparkSession.read
      .option("header","true")
      .option("inferSchema","true")
      .csv("src/main/resources/CyclePowerPlantDataSet.csv")
      .persist

    sparkSession.close()
  }
}
