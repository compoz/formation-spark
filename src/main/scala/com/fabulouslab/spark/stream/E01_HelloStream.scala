package com.fabulouslab.spark.stream

import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe



object E01_HelloStream {

  def main(args: Array[String]) {

    /**
      * En recoi en temps reel via Kafka, les cliques, chaque clique est compose de id du user (=clé)
      *   et URL sur la quelle il a cliqué (=valeur)
      *   - Affichez la clé et la valeur dans la console, en streamant     sparkSession.sparkContext.setLogLevel("ERROR")
 le serveur Kafka.
      * */

    val sparkSession = SparkSession.builder
      .master("local[1]")
      .appName("exo-3")
      .getOrCreate()
    sparkSession.sparkContext.setLogLevel("ERROR")

    import sparkSession.implicits._
    val kafkaStream = sparkSession
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "149.202.47.184:9092")
      .option("subscribe", "topic7")
      .load()

    val term = kafkaStream.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
      .as[(String, String)]
      .writeStream
      .format("console")
      .start()

    term.awaitTermination()

  }

}
