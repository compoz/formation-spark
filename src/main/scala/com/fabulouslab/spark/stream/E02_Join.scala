package com.fabulouslab.spark.stream

import org.apache.spark.sql.SparkSession

case class User(key : String, name : String)

object E02_Join {

  def main(args: Array[String]) {

    /**
     *   GetUserDs est une fonction qui retourne un dataset[User],
     *   qui contient id du user(= key) et son nom ( voir la classe User).
     *    - En faisant une jointure entre le stream, et le dataset[User],
      *    affichez dans la console, le nom du user et les liens sur lequel il a cliqué      *   - Affichez la clé et la valeur dans la console, en streamant depuis le serveur Kafka.
      *  * */

    implicit val sparkSession = SparkSession.builder
      .master("local[1]")
      .appName("exo-3")
      .getOrCreate()

    import sparkSession.implicits._

    val users = getUserDS

    val kafkaStream = sparkSession
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "localhost:9092")
      .option("subscribe", "topic7")
      .load()
  }

  def getUserDS(implicit sparkSession: SparkSession) = {
    import sparkSession.implicits._
    (0 to 10)
      .map(i => User(s"user$i",s"nom$i")).toDS
  }

}
