package com.fabulouslab.spark.stream

import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.streaming.OutputMode
import org.apache.spark.sql.streaming.OutputMode.Complete

case class Click(key : String, value : String)

object E03_StreamAGG {

  def main(args: Array[String]) {

    /**
      * - En utilisant le Watermark, calculez le nombre de cliques  par user et par minute.
      * */
    val sparkSession = SparkSession.builder
    .master("local[1]")
    .appName("exo-3")
    .getOrCreate()

    sparkSession.sparkContext.setLogLevel("ERROR")

    import sparkSession.implicits._


    val kafkaStream = sparkSession
    .readStream
    .format("kafka")
    .option("subscribe", "topic7")
    .load()

  }

}
